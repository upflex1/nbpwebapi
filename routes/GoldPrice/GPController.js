const NbpApi =require('../../NBP_API/NBPAPI');
const _helper = require('../../_helper');
var addDays = require('date-fns/addDays')
var parseISO = require('date-fns/parseISO');
var isBefore = require('date-fns/isBefore')
var format = require('date-fns/format');
const Joi = require('joi')
const { Boom } = require('@hapi/boom');

async function getPagingCollactionFromNBPApi(startDate, endDate){
    let finalDate = addDays(parseISO(startDate), 365);
    let initialDate = parseISO(startDate);
    let collection = [];
    let finalDatePlaceHolder = "";
    let initialDatePlaceHolder = "";
    while(isBefore(finalDate, parseISO(endDate))){
        finalDatePlaceHolder =format(finalDate, 'yyyy-MM-dd');
        initialDatePlaceHolder = format(initialDate,'yyyy-MM-dd');
        p = NbpApi.getGoldPriceTableDateRange(initialDatePlaceHolder, finalDatePlaceHolder ); 
        let response = await p;
        collection = [...collection, ...response.data];
        initialDate = addDays(finalDate, 1);
        finalDate = addDays(finalDate, 365);   
    }
    p = NbpApi.getGoldPriceTableDateRange( finalDatePlaceHolder, endDate);
    let response = await p;
    collection = [...collection, ...response.data];
    return collection;
}

async function getGoldLowestAndHighestPriceValidation(ctx, next){
    const schema = Joi.object({
            startDate: Joi.date()
                .iso()
                .required(),
    
            endDate: Joi.date()
                    .iso()
                    .required()
    }).with('startDate','endDate');

    let valRes = schema.validate(ctx.params);
    if(valRes.error){
        ctx.status = valRes.error.code || 400;
        ctx.body= valRes.error.message;
        return;
    }

    await next(ctx);
}

function calculateProfitFromInvestment(ctx){
    let investment = parseFloat(ctx.body.investment); 
    let purchase =  investment / ctx.body.lowest.price;
    let sale = purchase * ctx.body.highest.price;
    return sale - investment;
}

async function getGoldCurrentPrice(ctx){
        let p = NbpApi.getCurrentGoldPrice();
        let val = await p;
        if(val.status >= 200 && val.status < 300){
            ctx.status = 200;
    
            let body = {
                date: val.data[0].data,
                price: val.data[0].cena
            }
            ctx.body = body;
        }else{
            ctx.body = new Boom.badRequest(val);
        }

}


async function getGoldLowestAndHighestPrice(ctx){
    let startDate = ctx.params.startDate;
    let endDate = ctx.params.endDate;
    let p = undefined;
    if(_helper.isDifferenceInDaysLessThan365Days(startDate, endDate)){
        p = NbpApi.getGoldPriceTableDateRange(startDate, endDate);
        let response = await p;
        let val = _helper.getLowestAndHighestGoldPrice(response.data);
        ctx.body = val;
    }else{
        let f = getPagingCollactionFromNBPApi(startDate, endDate);
        let pageCollection = await f; 
        let val = _helper.getLowestAndHighestGoldPrice(pageCollection);
        ctx.body = val;
    }
}

async function getProfitFromInvestmentValidation(ctx, next){
    const schema = Joi.object({
        investment: Joi 
                    .number()
                    .required(),
        startDate: Joi.date()
            .iso()
            .required(),
        endDate: Joi.date()
                .iso()
                .required()
}).with('startDate','endDate');

let valRes = schema.validate(ctx.params);
if(valRes.error){
    ctx.status = valRes.error.code || 400;
    ctx.body= valRes.error.message;
    return;
}
    await next(ctx);
}
async function getProfitFromInvestment(ctx){
        let ctxMock = {
            params:{
                startDate : ctx.params.startDate,
                endDate : ctx.params.endDate,
            }
        };

        await getGoldLowestAndHighestPrice(ctxMock);
        ctxMock.body.investment =  parseFloat(ctx.params.investment);
    
        ctxMock.body.profit = calculateProfitFromInvestment(ctxMock);
        ctx.body= ctxMock.body;

}
async function hello(ctx){
        ctx.body = "Api is ready. Hello";
}

module.exports = {
    hello, 
    getProfitFromInvestment,
    getGoldLowestAndHighestPrice,
    getGoldCurrentPrice,
    getGoldLowestAndHighestPriceValidation,
    getProfitFromInvestmentValidation
}
        