const app = require('../../../server');
const supertest = require('supertest');
let request = supertest(app.listen());


it('Testing to see if Jest works!', () => {
    expect(2).toBe(2)
});



it('Get the Hello endpoint', async () => {    
    const res = await request.get('/');
    
    expect(res.status).toBe(200);
   // console.log(res);
    expect(res.text).toBe('Api is ready. Hello');
});

it('Get the lowest and highest Gold Price', async () => {
    const res = await request.get('/GoldLowestandHighestPrice/2021-01-01/2022-01-01');
    expect(res.status).toBe(200);
});

it('Validate params GetLowestAndHighestGoldPrice', async () => {
    const res = await request.get('/GoldLowestandHighestPrice/nada/2022-01-01');
    expect(res.status).toBe(400);
});

it('GetProfitFromInvestment in a given date', async () => {
    const res = await request.get('/getProfitFromInvestment/100000/2021-05-01/2022-01-01');
    expect(res.status).toBe(200);
});

it('Validate params in GetProfitFromInvestment', async () => {
    const res = await request.get('/getProfitFromInvestment/cien/2021-05-01/2022-01-01');
    expect(res.status).toBe(400);
});



