const router = require('@koa/router')();
var GPController = require('./GoldPrice/GPController');

const route = router.get('/', GPController.hello)
.get('/GoldCurrentPrice', GPController.getGoldCurrentPrice)
.get('/GoldLowestandHighestPrice/:startDate/:endDate', GPController.getGoldLowestAndHighestPriceValidation,GPController.getGoldLowestAndHighestPrice)
.get('/getProfitFromInvestment/:investment/:startDate/:endDate', GPController.getProfitFromInvestmentValidation, GPController.getProfitFromInvestment);

module.exports = {
  route
};
