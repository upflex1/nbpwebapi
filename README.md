# NBPWebApi



## Getting started
This is a project for a code challenge in Code Exito. 
This API is about to request the NBP API to get the price of GOLD in the past years, and calculate the best investment approach with a given investment amount. 

## EndPoints 

### Hello from Server.
```
127.0.0.1:3000/
```

### Current gold price. 
```
127.0.0.1:3000/GoldCurrentPrice
```
Response:
| Key | Value     |
| :-------- | :------- | 
| **date** | <Date_of_price> |
| **price** | <Gold_Price> | 

### Request the Lowest and Highest Gold Price in a given date range. 
**Params**
| Key | Value     |
| :-------- | :------- | 
| **startDate** | "yyyy-MM-dd" |
| **endDate** | "yyyy-MM-dd" | 


```
127.0.0.1:3000/GoldLowestandHighestPrice/{startDate}/{endDate}
```
Response:
**Lowest and Highest**
| Key | Value     |
| :-------- | :------- | 
| **date** | <Date_of_price> |
| **price** | <Gold_Price> | 


### Request the profit according to an investment amount, in a given date range. 
```
127.0.0.1:3000//getProfitFromInvestment/{investment}/{startDate}/{endDate}
```
Response:
**Lowest**: lowest gold price and date in the given date range. 
| Key | Value     |
| :-------- | :------- | 
| **date** | <Date_of_price> |
| **price** | <Gold_Price> | 

**Highest**: highest gold price and date in the given date range. 
| Key | Value     |
| :-------- | :------- | 
| **date** | <Date_of_price> |
| **price** | <Gold_Price> | 

**Investment**: investment amount. 

**Profit**: Calculated profit.

## To run it locally. 
This project is dockerized, to run it locally you must:

### First: Download or clone it.
### Second: Run it through docker.
To run it you must enter these commands in root directory of this project:
```
docker build -t <name:tag> .
docker run -d -p 3000:3000 <name:tag>
```

Try it in your local machine with this url: **127.0.0.1:3000/**

## Tests
To test this project you must run the next command: 
```
npm run test:watch
```


---
## Authors 
Luis Carlos Isaula
