const axios = require('axios');
const { application } = require('express');
const { json } = require('body-parser');

module.exports = {

    getCurrentGoldPrice: function (){
        const config = {
            headers :{
                Accept: application/json
            }
        }

        return axios
            .get('http://api.nbp.pl/api/cenyzlota', config).then(res => {
                let response = {
                    data: {
                        date: res.data.data,
                        price: res.data.cena
                    }
                }
                return res;
            }).catch(error => {
                return error;
            });
    },


    getGoldPriceTableDateRange: function (startDate, endDate){
        const config = {
            headers :{
                Accept: application/json
            }
        }
        let url = `http://api.nbp.pl/api/cenyzlota/${startDate}/${endDate}`;
        console.log(url);
        return axios
            .get(url, config).then(res => {
                return res;
            }).catch(error => {
                return error;
            });

    }
}
