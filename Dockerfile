# syntax=docker/dockerfile:1
FROM node:current  as builder
WORKDIR /app/build
ADD . .
RUN npm install

FROM node:current 
WORKDIR /srv
COPY --from=builder /app/build/ ./

EXPOSE 3000
CMD ["node", "start.js"]
