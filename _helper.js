var isAfter = require('date-fns/isAfter')
var differenceInDays = require('date-fns/differenceInDays')
var parseISO = require('date-fns/parseISO')

module.exports = {
    getLowestAndHighestGoldPrice: function (collection){
        let sortCollection = collection.sort((a, b) => a.cena-b.cena);
        let smallest = sortCollection[0];
        let highestCollection = sortCollection.filter(item => isAfter(parseISO(item.data), parseISO(smallest.data)));
        
        let highestVal = highestCollection[highestCollection.length - 1];
        let struc = {
            lowest: {
                date:smallest.data,
                price:smallest.cena,
            },
            highest: {
                date: highestVal.data,
                price:  highestVal.cena
            }
        }
        return struc;
    },
    isDifferenceInDaysLessThan365Days: function (startDate, endDate){
        let valDiffInDays =differenceInDays(parseISO(endDate),parseISO(startDate)); 
        let res = valDiffInDays <= 365;
        return res;
    }
}