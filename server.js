const koa = require('koa');

const Boom = require('@hapi/boom');
const {route} = require('./routes/router');
const bodyParser = require('koa-bodyparser');

const app = new koa();
app.use(async (ctx, next) => {
    try {
      await next();
    } catch (err) {
      ctx.status = err.status || err.code || err.statusCode || 500;
      ctx.body = {
        success: false,
        message: err.message,
      };
    }
  });
 
app.use(bodyParser());
app.use(route.routes())
   .use(route.allowedMethods({
    throw: true,
    notImplemented: () => new Boom.notImplemented(),
    methodNotAllowed: () => new Boom.methodNotAllowed()
  }));

module.exports = app

